# Rapid Prototyping of Analoge Sensor Systems 2021

This archive tries to sum up all curriculum of DTU course [22433 Rapid Prototyping of Analoge Sensor Systems](https://kurser.dtu.dk/course/22433) 2021.

To fully understand code comments, roadmap ect. you should be a part of the DTU course 22433 and therefor have access to the study materials. 

## Installation
- [Python](https://www.python.org/) 3.5 or higher
- [Maple](https://www.maplesoft.com/products/Maple/) 2021 or higher
- [Veecad](https://veecad.com/) 2.42 or higher
- [Multisim](https://www.ni.com/da-dk/support/downloads/software-products/download.multisim.html#312060) 14.2 or higher


## Support
If there are missing anything, then feel free to submit an [issue](https://gitlab.com/kiwimarc/rapid-prototyping-2021/-/issues) or send me an [email](mailto:incoming+kiwimarc-rapid-prototyping-2021-31895364-u09mi3geqn24e1fxx1n7vyc7-issue@incoming.gitlab.com).

## Roadmap
* [x] Week 1
    * [x] Ohms law
***  
* [x] Week 2
    * [x] Kirchshoff law
    * [x] Voltage divider
    * [x] Current divider
***
* [x] Week 3
    * [x] Nodal analysis
***
* [x] Week 4 
    * [x] Loop analysis
***
* [x] Week 5
    * [x] Linearity method
    * [x] Thevenin theorems
    * [x] Norton theorems
    * [x] Current superposition
***
* [ ] Week 6
    * [ ] inverting op-amp
    * [ ] Ideal op-amp
***
* [ ] Week 7
    * [ ] non-inverting op-amp
    * [ ] Summing amplifier
***
* [ ] Week 8
    * [ ] Thermistor
    * [ ] Photoresistor
***
* [ ] Week 9
    * [ ] Pressure sensor
    * [ ] Differential amplifier
***
* [ ] Week 10
    * [ ] Sinusoidal signals
    * [ ] Fourier series
***
* [ ] Week 11
    * [ ] Capacitors
    * [ ] Inductors
***
* [ ] Week 12
    * [ ] Active lowpass filter, 1st order
    * [ ] Active highpass filter, 1st order
***
* [ ] Week 13
    * [ ] Sallen-Key 2nd order lowpass filter
    * [ ] Butterworth filter performance equations
    * [ ] Sensitivity analysis
    * [ ] Constrained filter design
    * [ ] Sallen-Key 2nd order highpass filter with low sensitivity


## Authors and acknowledgment
For every formula used in this project is from the DTU course 22433 and is therefor credited to [Kaj-Åge Henneberg](https://www.dtu.dk/service/telefonbog/person?id=5548&cpid=&tab=1)

## License
[MIT License](https://gitlab.com/kiwimarc/rapid-prototyping-2021/-/blob/main/LICENSE)

Feel free to use this project in anyway you want.

## Project status
This project will be worked on until the 16th of december 2021. After this date only contributions and suggestions will be added.
